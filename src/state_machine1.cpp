#include <Controllino.h>
#include "common.hpp"

extern struct SETPTS setpoints;
extern bool safety_disable;
struct SM1DATA sm1data;

// valve structs
extern struct valve V1;
extern struct valve V2;
extern struct valve V3;
extern struct valve V4;
extern struct valve V5;
extern struct valve PSU;

// emergency handler for source protection
void proc_sm1(void){
    
    // ignore safety check if safety is disabled by the user
    if (safety_disable){
        return;
    }

    switch(sm1data.state){
        case 0:
            sm1data.safety_active = true;
            sm1data.emerg_precedure = false;
            if (!setpoints.spexpv){
                sm1data.emerg_precedure = true;
            }
            sm1data.state = 1;
            break;
        case 1:
            if (!setpoints.spexpv && setpoints.spmpv){
                sm1data.emerg_precedure = true;
                sm1data.state = 2;
            } 
            if (setpoints.spexpv){
                sm1data.emerg_precedure = false;
            }
            break;
        case 2:
            V1.request = 0;
            V2.request = 0;
            PSU.request = 0;
            if (!setpoints.spllpv){
                V3.request = 0;
                V4.request = 1;
                sm1data.state = 3;
            } else {
                sm1data.state = 4;
            }
            break;
        case 3:
            if (setpoints.spllpv){
                sm1data.state = 4;
            }
            break;
        case 4:
            V3.request = 1;
            V4.request = 0;
            V5.request = 1;
            sm1data.state = 5;
            break;
        case 5:
            if(!setpoints.spllvent){
                sm1data.state = 0;
            }
        default:
            sm1data.state = 0;
    }
}