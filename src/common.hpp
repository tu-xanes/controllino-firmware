#include <Controllino.h>
#include <SPI.h>
#include <Ethernet.h>

#define BUFFER_LEN 32
#define SERVERPORT 23

struct valve
{
  uint8_t addout;
  uint8_t addlsneg;
  uint8_t addlspos;
  uint8_t request;
  uint8_t state;
  uint8_t disable;
  uint8_t addextra;
};

struct SETPTS
{
  bool spmpv;
  bool spmhv;
  bool spllpv;
  bool spllhv;
  bool spexpv;
  bool spllvent;
};

struct SM1DATA
{
  uint16_t state;
  bool safety_active;
  bool emerg_precedure;
};

struct SM2DATA
{
  uint16_t state;
  bool timerdone;
};

struct SM3DATA
{
  uint16_t state;
  uint8_t led_vent_rate;
  uint8_t led_evac_rate;
  bool slowtick;
  bool fasttick;
  bool led_vent;
  bool led_evac;
  bool reset;
};

struct BTDATA
{
  uint8_t state;
  bool pressed;
  bool disable;
  bool vbutton;
};

// support functions
unsigned int get_command(EthernetClient* client, char* Buffer, unsigned char MaxLen);
void get_response(char* cmdbuf, char* respbuf, int len);
void mergebits(long* outval, uint8_t input, uint8_t bitpos);
void setupIO(void);
void readallin(long* outval);
void readallout(long* outval);
void readvalves(long* outval);
void setupvalvemap(void);
bool processcond(struct valve* v, bool SETC, bool RESC, bool FSET, bool FRES, bool SDIS);
void process_valves(void);
void update_setpoints(void);
void proc_sm1(void);
void proc_sm2(void);
void proc_sm3(void);
void setupsm3timers(void);
void CheckHVSafety(void); //[D]