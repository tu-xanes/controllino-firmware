#include <Controllino.h>
#include <SPI.h>
#include <Ethernet.h>
#include "common.hpp"

// IO mapping
#define lldoor_addr CONTROLLINO_IN0
#define llstick_addr CONTROLLINO_A10

// global variables
extern bool safety_disable;
extern struct valve V1;
extern struct valve V2;
extern struct valve V3;
extern struct valve V4;
extern struct valve V5;
extern struct valve PSU;
extern struct SETPTS setpoints;

// V1
void process_v1(void){
    struct valve *V = &V1;
    // update limit switch status
    // bool LSNEG = digitalRead(V->addlsneg);
    // bool LSPOS = digitalRead(V->addlspos);

    // ## valve rules ##
    bool set_cond = true;
    bool reset_cond = true;
    bool force_set = false;
    bool force_reset = false;

    // process rules
    if (processcond(V, set_cond, reset_cond, force_set, force_reset, safety_disable)){
        digitalWrite(V->addout, V->state);    
    }
}

// V2
void process_v2(void){
    struct valve *V = &V2;
    // update limit switch status
    // bool LSNEG = digitalRead(V->addlsneg);
    // bool LSPOS = digitalRead(V->addlspos);

    // ## valve rules ##
    bool set_cond = true;
    bool reset_cond = true;
    bool force_set = false;
    bool force_reset = false;

    // process rules
    if (processcond(V, set_cond, reset_cond, force_set, force_reset, safety_disable)){
        digitalWrite(V->addout, V->state);    
    }
}

// V3
void process_v3(void){
    struct valve *V = &V3;
    // update limit switch status
    // bool LSNEG = digitalRead(V->addlsneg);
    // bool LSPOS = digitalRead(V->addlspos);

    // ## valve rules ##
    bool set_cond = true;
    bool reset_cond = true;
    bool force_set = false;
    bool force_reset = false;

    // process rules
    if (processcond(V, set_cond, reset_cond, force_set, force_reset, safety_disable)){
        digitalWrite(V->addout, V->state);    
    }
}

// V4
void process_v4(void){
    struct valve *V = &V4;
    // update limit switch status
    // bool LSNEG = digitalRead(V->addlsneg);
    // bool LSPOS = digitalRead(V->addlspos);
    // bool lldoorclosed = digitalRead(lldoor_addr);

    // ## valve rules ##
    bool set_cond = true;
    bool reset_cond = true;
    bool force_set = false;
    bool force_reset = false;

    // process rules
    if (processcond(V, set_cond, reset_cond, force_set, force_reset, safety_disable)){
        // state machine 2 handles the actual output
        // digitalWrite(V->addout, V->state);    
    }
}

// V5
void process_v5(void){
    struct valve *V = &V5;
    // update limit switch status
    // bool LSNEG = digitalRead(V->addlsneg);
    // bool LSPOS = digitalRead(V->addlspos);
    bool llstickout = digitalRead(llstick_addr);

    // ## valve rules ##
    bool set_cond =  (setpoints.spllpv == setpoints.spmpv) and (setpoints.spllhv == setpoints.spmhv);
    bool reset_cond = llstickout;
    bool force_set = false;
    bool force_reset = false;

    // process rules
    if (processcond(V, set_cond, reset_cond, force_set, force_reset, safety_disable)){
        digitalWrite(V->addout, V->state);
        digitalWrite(V->addextra, !(V->state));  
    }
}

// PSU enable/disable
void process_psu(void){
    struct valve *V = &PSU;

    // ## valve rules ##
    bool set_cond = true;
    bool reset_cond = true;
    bool force_set = false;
    bool force_reset = false;

    // process rules
    if (processcond(V, set_cond, reset_cond, force_set, force_reset, safety_disable)){
        digitalWrite(V->addout, V->state);    
    }
}

// ## rules logic functions ##
bool processcond(struct valve* v, bool SETC, bool RESC, bool FSET, bool FRES, bool SDIS){
    bool update=false;
    // safety disable
    if (SDIS){
        update=true;
        v->disable=false;
        v->state = v->request;
    } else {  
        // othersise update disable status
        if (v->state){
            v->disable = !RESC;
        } else {
            v->disable = !SETC;
        }
        // then check conditions by priority
        if (FRES && (v->state)){
            // force reset
            v->request=false;
            v->state=false;
            update=true;
        } else if (FSET && !(v->state)){
            // force set
            v->request=true;
            v->state=true;
            update = true;
        } else if (!(v->request) && (v->state) && (RESC) && !(v->disable)){
            v->state=false;
            update = true;
        } else if ((v->request) && !(v->state) && (SETC) && !(v->disable)){
            v->state=true;
            update = true;
        }
    }
    v->request=v->state;
    return update;
}

void process_valves(void){
    process_v1();
    process_v2();
    process_v3();
    process_v4();
    process_v5();
    process_psu();
}