#include <Controllino.h>
#include "common.hpp"
#include <arduino-timer.h>

#define slow_interval 500
#define fast_interval 125
#define btdisable_interval 2000
#define led_evac_addr CONTROLLINO_D11
#define led_vent_addr CONTROLLINO_D10
#define button_addr CONTROLLINO_IN1
#define stick_add CONTROLLINO_A10
#define door_addr CONTROLLINO_IN0

// variables
struct SM3DATA sm3data;
struct BTDATA btdata;

extern struct SETPTS setpoints;
extern struct valve V1;
extern struct valve V3;
extern struct valve V4;
extern struct valve V5;

// timers
auto timer2 = timer_create_default();
auto timer3 = timer_create_default();
auto timer4 = timer_create_default();

bool toggle_slow(void *){
    sm3data.slowtick = !sm3data.slowtick;
    return true;
}

bool toggle_fast(void *){
    sm3data.fasttick = !sm3data.fasttick;
    return true;
}

bool timer4done(void *){
    btdata.disable = false;
    return false;
}

void setupsm3timers(void){
    timer2.every(slow_interval, toggle_slow);
    timer3.every(fast_interval, toggle_fast);
    sm3data.led_evac_rate = 0;
    sm3data.led_vent_rate = 0;
    sm3data.slowtick = false;
    sm3data.fasttick = false;
}

bool button_pressed(void){
    if (btdata.pressed and !btdata.disable){
        btdata.pressed = false;
        btdata.disable = true;
        return true;
    } else {
        return false;
    }
}

void proc_button(void){
    bool bt_pressed = !digitalRead(button_addr) or btdata.vbutton;

    switch (btdata.state){
        case 0:
            btdata.pressed = false;
            btdata.disable = false;
            btdata.vbutton = false;
            btdata.state = 1;
            break;
        case 1:
            if(bt_pressed){
                btdata.pressed = true;
                btdata.disable = false;
                btdata.state = 2;
            }
            break;
        case 2:
            if(btdata.disable){
                btdata.pressed = false;
                btdata.vbutton = false;
                timer4.in(btdisable_interval, timer4done);
                btdata.state = 3;
            }
            break;
        case 3:
            if(!btdata.disable){
                btdata.state = 0;
            }
            break;
        default:
            btdata.state = 0;
    }

}

void ledtick(void){
    switch (sm3data.led_evac_rate){
        case 0:
            digitalWrite(led_evac_addr, 1);
            break;
        case 1:
            digitalWrite(led_evac_addr, 0);
            break;
        case 2:
            digitalWrite(led_evac_addr, sm3data.slowtick);
            break;
        case 3:
            digitalWrite(led_evac_addr, sm3data.fasttick);
            break;
        default:
            sm3data.led_evac_rate = 0;
            break;
    }

    switch (sm3data.led_vent_rate){
        case 0:
            digitalWrite(led_vent_addr, 1);
            break;
        case 1:
            digitalWrite(led_vent_addr, 0);
            break;
        case 2:
            digitalWrite(led_vent_addr, sm3data.slowtick);
            break;
        case 3:
            digitalWrite(led_vent_addr, sm3data.fasttick);
            break;
        default:
            sm3data.led_vent_rate = 0;
            break;
    }
}

void proc_test(void){
    timer2.tick();
    timer3.tick();
    timer4.tick();

    ledtick();
    proc_button();

    switch (sm3data.state){
        case 0:
            sm3data.led_evac_rate = 0;
            sm3data.led_vent_rate = 0;
            sm3data.reset = false;
            sm3data.state = 1;
            break;
        case 1:
            if(button_pressed()){
                sm3data.led_evac_rate = 1;
                sm3data.led_vent_rate = 0;
                sm3data.state = 2;
            }
            if (sm3data.reset){
                sm3data.state=0;
            }
            break;
        case 2:
            if(button_pressed()){
                sm3data.led_evac_rate = 0;
                sm3data.led_vent_rate = 1;
                sm3data.state = 1;
            }
            if (sm3data.reset){
                sm3data.state=0;
            }
            break;
        default:
            sm3data.state = 0;
            break;
    }
}

void proc_sm3(void){
    timer2.tick();
    timer3.tick();
    timer4.tick();

    ledtick();
    proc_button();

    bool stick_ok = digitalRead(stick_add);
    bool door_closed = digitalRead(door_addr);
    bool v1closed = digitalRead(V1.addlsneg);
    bool v5closed = digitalRead(V5.addlsneg);

    switch (sm3data.state){
        case 0:
            sm3data.led_evac_rate = 0;
            sm3data.led_vent_rate = 0;
            sm3data.reset = false;
            if (setpoints.spllvent){
                sm3data.state = 1;
            } else {
                sm3data.state = 10;
            }
            break;
        // evacuated and ready to vent
        case 1:
            sm3data.led_evac_rate = 1;
            sm3data.state = 2;
            break;
        // waiting for button
        case 2:
            if (button_pressed()){
                sm3data.led_evac_rate = 0;
                sm3data.led_vent_rate = 3;
                sm3data.state = 3;
            }else if (sm3data.reset || !setpoints.spllvent){
                sm3data.state=0;
            }
            break;
        // wait for stick out
        case 3:
            if (stick_ok){
                V1.request = 0;
                V5.request = 0;
                sm3data.led_vent_rate = 2;
                sm3data.state = 4;
            }
            if (sm3data.reset || button_pressed()){
                sm3data.state=0;
            }
            break;
        // wait for v1 and v5 closed states
        case 4:
            V5.request = 0;
            if (v1closed && v5closed){
                V3.request = 1;
                sm3data.state = 5;
            }
            if (sm3data.reset || button_pressed()){
                sm3data.state=0;
            }
            break;
        case 5:
            if (!setpoints.spllvent){
                sm3data.state = 0;
            }
            if (sm3data.reset || button_pressed()){
                sm3data.state=0;
            }
            break;
        // vented and ready to evacuate
        case 10:
            sm3data.led_vent_rate = 1;
            sm3data.state = 11;
            break;
        // wait for button press
        case 11:
            if (button_pressed()){
                sm3data.led_vent_rate = 0;
                sm3data.led_evac_rate = 3;
                sm3data.state = 12;
            }else if (sm3data.reset || setpoints.spllvent){
                sm3data.state=0;
            }
            break;
        // check for door closed
        case 12:
            if (door_closed){
                V3.request = 0;
                V4.request = 1;
                sm3data.led_evac_rate = 2;
                sm3data.state = 13;
            } 
            if (sm3data.reset || button_pressed()){
                sm3data.state=0;
            }
            break;
        // waits for load lock pre vacuum
        case 13:
            if (setpoints.spllpv){
                V4.request = 0;
                V1.request = 1;
                sm3data.state = 14;
            }
            if (sm3data.reset || button_pressed()){
                sm3data.state=0;
            }
            break;
        // waits for load lock high vacuum
        case 14:
            if (setpoints.spllhv){
                V5.request = 1;
                sm3data.state = 0;
            }
            if (sm3data.reset || button_pressed()){
                sm3data.state=0;
            }
            break;
        default:
            sm3data.state = 0;
            break;
    }
}