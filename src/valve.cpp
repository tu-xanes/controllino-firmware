#include <Controllino.h>
#include <SPI.h>
#include <Ethernet.h>
#include "common.hpp"


// Global vars
extern bool safety_disable;
extern struct SM1DATA sm1data;
extern struct BTDATA btdata;

// valve structs
struct valve V1;
struct valve V2;
struct valve V3;
struct valve V4;
struct valve V5;
struct valve PSU;

// sample stick limit switch
uint8_t SampleLSaddr=CONTROLLINO_A10;

void setupvalvemap(){
    // V1
  V1.addout = CONTROLLINO_D0;
  V1.addlsneg = CONTROLLINO_A0;
  V1.addlspos = CONTROLLINO_A1;
  // V2
  V2.addout = CONTROLLINO_D1;
  V2.addlsneg = CONTROLLINO_A2;
  V2.addlspos = CONTROLLINO_A3;
  // V3
  V3.addout = CONTROLLINO_D2;
  V3.addlsneg = CONTROLLINO_A4;
  V3.addlspos = CONTROLLINO_A5;
  // V4
  V4.addout = CONTROLLINO_D3;
  V4.addlsneg = CONTROLLINO_A6;
  V4.addlspos = CONTROLLINO_A7;
  // V5
  V5.addout = CONTROLLINO_D5;
  V5.addlsneg = CONTROLLINO_A9;
  V5.addlspos = CONTROLLINO_A8;
  V5.addextra = CONTROLLINO_D4;
  // PSU
  PSU.addout = CONTROLLINO_D7;
  PSU.addlsneg = 0;
  PSU.addlspos = 0;
}

void readvalves(long* outval){
    unsigned long i;
    *outval=0;

    i = V1.disable;
    *outval |= (i<<0);
    i = V2.disable;
    *outval |= (i<<1);
    i = V3.disable;
    *outval |= (i<<2);
    i = V4.disable;
    *outval |= (i<<3);
    i = V5.disable;
    *outval |= (i<<4);
    i = safety_disable;
    *outval |= (i<<5);
    i = PSU.disable;
    *outval |= (i<<6);
    i = sm1data.safety_active;
    *outval |= (i<<7);
    i = sm1data.emerg_precedure;
    *outval |= (i<<8);
    i = btdata.disable;
    *outval |= (i<<9);
}