#include <Controllino.h>
#include "common.hpp"

#define HV_Interlock CONTROLLINO_D12 //[D]
#define VacSwitch_Interlock CONTROLLINO_A14 //[D]
#define Excillium_Interlock CONTROLLINO_A11 //[D]

bool VacSwitch_Status;
bool Excillium_Status;

void CheckHVSafety(){
    VacSwitch_Status = digitalRead(VacSwitch_Interlock);
    Excillium_Status = digitalRead(Excillium_Interlock);
    if (VacSwitch_Status && Excillium_Status) {
        digitalWrite(HV_Interlock, HIGH);
    } else {
        digitalWrite(HV_Interlock, LOW);
    }
}