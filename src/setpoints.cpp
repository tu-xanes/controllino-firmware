#include <Controllino.h>
#include "common.hpp"

// IO mapping for maxigauge relays
#define RELAYA CONTROLLINO_A12
#define RELAYB CONTROLLINO_A13
#define RELAYC CONTROLLINO_A15
#define RELAYD CONTROLLINO_I16
#define RELAYE CONTROLLINO_I17
#define RELAYF CONTROLLINO_I18

struct SETPTS setpoints;

void update_setpoints(void){
    setpoints.spmpv = digitalRead(RELAYA);
    setpoints.spmhv = digitalRead(RELAYB);
    setpoints.spllpv = digitalRead(RELAYC);
    setpoints.spllhv = digitalRead(RELAYD);
    setpoints.spexpv = digitalRead(RELAYE);
    setpoints.spllvent = digitalRead(RELAYF);
}
