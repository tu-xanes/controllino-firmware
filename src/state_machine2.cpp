#include <Controllino.h>
#include "common.hpp"
#include <arduino-timer.h>

#define prepump_addr CONTROLLINO_D6
#define timer1_interval 500

// valve structs
extern struct valve V4;

struct SM2DATA sm2data;
auto timer1 = timer_create_default();

bool ftimer1_done(void *){
    sm2data.timerdone = true;
    return false;
}

// Handles V4 and pre pump 
void proc_sm2(void){
    timer1.tick();
    switch(sm2data.state){
        case 0:
            digitalWrite(V4.addout, 0);
            digitalWrite(prepump_addr, 0);
            sm2data.timerdone = false;
            sm2data.state = 1;
            break;
        case 1:
            if (V4.state){
                digitalWrite(prepump_addr, 1);
                timer1.in(timer1_interval, ftimer1_done);
                sm2data.state = 2;
            }
            break;
        case 2:
            if (sm2data.timerdone){
                sm2data.timerdone = false;
                if (V4.state){
                    digitalWrite(V4.addout, 1);
                    sm2data.state = 3;
                } else {
                    sm2data.state = 0;
                }
            }
            break;
        case 3:
            if (!V4.state){
                digitalWrite(V4.addout, 0);
                timer1.in(timer1_interval, ftimer1_done);
                sm2data.state = 4;
            }
            break;
        case 4:
            if (sm2data.timerdone){
                sm2data.timerdone = false;
                if (V4.state){
                    digitalWrite(V4.addout, 1);
                    sm2data.state = 3;
                } else {
                    digitalWrite(prepump_addr, 0);
                    sm2data.state = 0;
                }
            }
            break;
        default:
            sm2data.state = 0;
    } 
}

