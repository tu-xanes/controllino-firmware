#include <Controllino.h>
#include <SPI.h>
#include <Ethernet.h>
#include "common.hpp"

// Global vars
byte mac[] = { 0xD0, 0x09, 0x0B, 0x6E, 0x26, 0xEA };
EthernetServer server(SERVERPORT);
char COMMAND_BUFFER[BUFFER_LEN];
char RESP_BUFFER[BUFFER_LEN];

void setup() {
  // setup IOs
  setupIO();

  // setup valve map
  setupvalvemap();

  // create serial output 
  Serial.begin(9600);
  delay(500);
  Serial.println("\nSetup begin...");
  Serial.println("Controllino XANES");
  delay(1000);

  // get IP address from DHCP
  Serial.println("Getting IP Address...");
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to obtaining an IP address");
  }
  Serial.print("Arduino's IP Address: ");
  Serial.println(Ethernet.localIP());

  // start ethernet server listener
  server.begin();
  Serial.println("Setup OK. Running...");

  // Sets R15 = 1 to indicate setup done
  digitalWrite(CONTROLLINO_D23, true);

  // Sets D0 = true to force V1 close after controllino restart
  // digitalWrite(CONTROLLINO_D0, true);

  // setup timers on state machine 3
  setupsm3timers();

}

void loop() {
  //hancle server client connections
  EthernetClient client = server.available();
  if (client) {
    int len = get_command(&client, COMMAND_BUFFER, BUFFER_LEN);
    get_response(COMMAND_BUFFER, RESP_BUFFER, len);
    server.write(RESP_BUFFER);
  }

  // [D: HV safety check]
  CheckHVSafety();
  
  // update setpoints values
  update_setpoints();
  
  // process all valve rules
  process_valves();
  
  // process state machine 1 : source protection
  proc_sm1();

  // process state machine 2 : V4 and prepump logic
  proc_sm2();

  // process state machine 3 : Vent / Evac load lock 
  proc_sm3();
}

