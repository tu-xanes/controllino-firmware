#include <Controllino.h>
#include <SPI.h>
#include <Ethernet.h>
#include "common.hpp"

//  global safety disable variable
bool safety_disable;
uint8_t psu_switch_addr = CONTROLLINO_D8;

// global vars
extern struct valve V1;
extern struct valve V2;
extern struct valve V3;
extern struct valve V4;
extern struct valve V5;
extern struct valve PSU;
extern struct SM3DATA sm3data;
extern struct BTDATA btdata;

// 32 bit register for status output
long outval;

// monitored controllino inputs
uint8_t allinputs[] = {
  CONTROLLINO_A0,
  CONTROLLINO_A1,
  CONTROLLINO_A2,
  CONTROLLINO_A3,
  CONTROLLINO_A4,
  CONTROLLINO_A5,
  CONTROLLINO_A6,
  CONTROLLINO_A7,
  CONTROLLINO_A8,
  CONTROLLINO_A9,
  CONTROLLINO_A10,
  CONTROLLINO_A11,
  CONTROLLINO_A12,
  CONTROLLINO_A13,
  CONTROLLINO_A14,
  CONTROLLINO_A15,
  CONTROLLINO_I16,
  CONTROLLINO_I17,
  CONTROLLINO_I18,
  CONTROLLINO_IN0,
  CONTROLLINO_IN1
};

// monitored controllino outputs
uint8_t alloutputs[] = {
  CONTROLLINO_D0,
  CONTROLLINO_D1,
  CONTROLLINO_D2,
  CONTROLLINO_D3,
  CONTROLLINO_D4,
  CONTROLLINO_D5,
  CONTROLLINO_D6,
  CONTROLLINO_D7,
  CONTROLLINO_D8,
  CONTROLLINO_D9,
  CONTROLLINO_D10,
  CONTROLLINO_D11,
  CONTROLLINO_D12,
  CONTROLLINO_D13,
  CONTROLLINO_D14,
  CONTROLLINO_D15,
  CONTROLLINO_D16,
  CONTROLLINO_D17,
  CONTROLLINO_D18,
  CONTROLLINO_D19,
  CONTROLLINO_D20,
  CONTROLLINO_D21,
  CONTROLLINO_D22,
  CONTROLLINO_D23,
};

// Helper function to read command and put it in a buffer
unsigned int get_command(EthernetClient* client, char* Buffer, unsigned char MaxLen){
    unsigned int c;
    unsigned char idx = 0;
    
    while(client->available() && idx < MaxLen - 1){
        c = client->read();
        if ((unsigned char)c == '\n'){
            break;
        } else {
            Buffer[idx] = (unsigned char)c;
            idx++;
        }
    }
    Buffer[idx] = '\0';
    return idx;
}

// Simple command mapping
void get_response(char* cmdbuf, char* respbuf, int len){
  if (strcmp(cmdbuf, "statin?")==0){
    readallin(&outval);
    sprintf(respbuf, "statin:%lu\n", outval);
  } else if (strcmp(cmdbuf, "statout?")==0){
    readallout(&outval);
    sprintf(respbuf, "statout:%lu\n", outval);
  } else if (strcmp(cmdbuf, "statv?")==0){
    readvalves(&outval);
    sprintf(respbuf, "statv:%lu\n", outval);
    // V1
  } else if (strcmp(cmdbuf, "v1:0")==0){
    V1.request = 0;
    sprintf(respbuf, "v1:0!\n");
  } else if (strcmp(cmdbuf, "v1:1")==0){
    V1.request = 1;
    sprintf(respbuf, "v1:1!\n");
    // V2
   } else if (strcmp(cmdbuf, "v2:0")==0){
    V2.request = 0;
    sprintf(respbuf, "v2:0!\n");
  } else if (strcmp(cmdbuf, "v2:1")==0){
    V2.request = 1;
    sprintf(respbuf, "v2:1!\n");
    // V3
   } else if (strcmp(cmdbuf, "v3:0")==0){
    V3.request = 0;
    sprintf(respbuf, "v3:0!\n");
  } else if (strcmp(cmdbuf, "v3:1")==0){
    V3.request = 1;
    sprintf(respbuf, "v3:1!\n");
    // V4
   } else if (strcmp(cmdbuf, "v4:0")==0){
    V4.request = 0;
    sprintf(respbuf, "v4:0!\n");
  } else if (strcmp(cmdbuf, "v4:1")==0){
    V4.request = 1;
    sprintf(respbuf, "v4:1!\n");
    // V5
   } else if (strcmp(cmdbuf, "v5:0")==0){
    V5.request = 0;
    sprintf(respbuf, "v5:0!\n");
  } else if (strcmp(cmdbuf, "v5:1")==0){
    V5.request = 1;
    sprintf(respbuf, "v5:1!\n");
    // PSU
   } else if (strcmp(cmdbuf, "psu:0")==0){
    PSU.request = 0;
    sprintf(respbuf, "psu:0!\n");
  } else if (strcmp(cmdbuf, "psu:1")==0){
    PSU.request = 1;
    sprintf(respbuf, "psu:1!\n");
    // Cooler / heater
  } else if (strcmp(cmdbuf, "psusw:0")==0){
    digitalWrite(psu_switch_addr, 0);
    sprintf(respbuf, "psusw:0!\n");
  } else if (strcmp(cmdbuf, "psusw:1")==0){
    digitalWrite(psu_switch_addr, 1);
    sprintf(respbuf, "psusw:1!\n");     
    // ID
  } else if (strcmp(cmdbuf, "id?")==0){
    sprintf(respbuf, "Controllino XANES\n");  
    // safety state
  } else if (strcmp(cmdbuf, "sd:0")==0){
    safety_disable=0;
    sprintf(respbuf, "sd:0!\n");
  } else if (strcmp(cmdbuf, "sd:1")==0){
    safety_disable=1;
    sprintf(respbuf, "sd:1!\n");
    // reset evac/vent state machine 3
  } else if (strcmp(cmdbuf, "res3:0")==0){
    sm3data.reset = true;
    sprintf(respbuf, "res3:0!\n");
    // virtual button press
  } else if (strcmp(cmdbuf, "vbt")==0){
    btdata.vbutton = true;
    sprintf(respbuf, "vbt!\n");
    // default output
  } else {
    sprintf(respbuf, "?\n");
  }
}

// add bit value to a 32bit register 
//[D: this function is called within get_response and readallin/out to read A1.. D1 etc channels]
void mergebits(long* outval, uint8_t input, uint8_t bitpos){
  unsigned long i = digitalRead(input);
  *outval |= (i<<bitpos);
}

// read all input values into a 32bit register
void readallin(long* outval){
  *outval=0;
  int inputsize = 21;
  for(int i=0; i<inputsize; i++){
    mergebits(outval, allinputs[i], i);
  }
}

// read all output values into a 32bit register
void readallout(long* outval){
  *outval=0;
  int outputsize = 24;
  for(int i=0; i<outputsize; i++){
    mergebits(outval, alloutputs[i], i);
  }
}

// setup IO direction for inputs and outputs, 
//[D: pinMode configures a specific pin on the microcontroller to behave either as an input or an output]
void setupIO(void){
  int inputsize = 21;
  for(int i; i<inputsize; i++){
    pinMode(allinputs[i], INPUT);
  }
  int outputsize = 24;
  for(int i; i<outputsize; i++){
    pinMode(alloutputs[i], OUTPUT);
  }
}
